package com.sayeedm.reporttest;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by sayeedm on 8/18/16.
 */
public class ColumnInfo {

    private static HashMap<String, String> typeMapping;

    static {
        typeMapping = new HashMap<String, String>();
        typeMapping.put("NUMBER", Long.class.getName());
        typeMapping.put("VARCHAR2", String.class.getName());
        typeMapping.put("TIMESTAMP", Date.class.getName());
    }


    private String name = "" ;
    private String type = "" ;

    public ColumnInfo(String name, String type){
        this.name = name;
        this.type = typeMapping.get(type);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
