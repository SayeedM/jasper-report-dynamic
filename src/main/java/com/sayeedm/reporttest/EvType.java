package com.sayeedm.reporttest;

/**
 * Created by sayeedm on 8/17/16.
 */
public class EvType {

    public Long id;
    public String TYPE_EN;
    public String TYPE_RU;
    public String TYPE_TJ;

    public EvType(long id, String typeEn, String typeRu, String typeTj){
        this.id = id;
        this.TYPE_EN = typeEn;
        this.TYPE_RU = typeRu;
        this.TYPE_TJ = typeTj;
    }

    public Long getId(){
        return id;
    }

    public String getTYPE_RU(){
        return TYPE_RU;
    }

    public String getTYPE_EN(){
        return TYPE_EN;
    }

    public String getTYPE_TJ(){
        return TYPE_TJ;
    }
}
