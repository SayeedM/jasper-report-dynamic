package com.sayeedm.reporttest;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


/**
 *
 * Created by sayeedm on 8/17/16.
 */
public class Main {

    private JFrame mainFrame;
    private JTextArea queryText;
    private JTextArea titleText;

    public static void main(String [] args) {
        Main m = new Main();
        m.prepareGUI();
        m.showEventDemo();
    }


    private void prepareGUI(){
        mainFrame = new JFrame("ReportGen");
        mainFrame.setSize(500, 600);
        mainFrame.setLayout(null);

        JLabel headerLabel = new JLabel("Query", JLabel.LEFT );
        headerLabel.setBounds(10, 10, 100, 20);
        mainFrame.add(headerLabel);

        queryText = new JTextArea(500, 500);
        queryText.setBounds(10, 45, 480, 300);
        queryText.setText("select code, type_en, type_ru, type_tj from ev_type");
        mainFrame.add(queryText);

        titleText = new JTextArea(500, 50);
        titleText.setBounds(10, 360, 480, 30);
        titleText.setText("Title Parameter");
        mainFrame.add(titleText);

        JButton submitButton = new JButton("Gen JRXML");
        submitButton.setBounds(10, 410, 150, 30);
        submitButton.setActionCommand("Submit");
        submitButton.addActionListener(new ButtonClickListener());
        mainFrame.add(submitButton);

        JButton submitButton2 = new JButton("Generate from 1.jrxml");
        submitButton2.setBounds(170, 410, 250, 30);
        submitButton2.setActionCommand("Submit2");
        submitButton2.addActionListener(new ButtonClickListener());
        mainFrame.add(submitButton2);

        JLabel infoLabel = new JLabel("Report 1.pdf and 1.jrxml will be generated in the current directory of jar");
        infoLabel.setBounds(10, 460, 450, 25);
        mainFrame.add(infoLabel);


        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
        mainFrame.setVisible(true);
    }

    private void showEventDemo(){




        mainFrame.setVisible(true);
    }

    private class ButtonClickListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String command = e.getActionCommand();

            if( command.equals( "Submit" ) )  {
                String query = queryText.getText();
                String title = titleText.getText();
                JasperGen gen = new JasperGen();
                gen.gen(query, title);
            }

            if( command.equals( "Submit2" ) )  {
                String query = queryText.getText();
                String title = titleText.getText();
                JasperGen gen = new JasperGen();
                gen.generateFromJrxml(title, "1.jrxml");
            }

        }
    }





}
