package com.sayeedm.reporttest;

import ar.com.fdvs.dj.core.DJConstants;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.AutoText;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.StyleBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.entities.Parameter;
import net.sf.dynamicreports.jasper.base.JasperReportDesign;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.xml.JRGenericPrintElementParameterFactory;
import net.sf.jasperreports.view.JasperViewer;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sayeedm on 8/18/16.
 */
public class JasperGen {


    public void gen(String query, String title){
        System.out.println("-------- Oracle JDBC Connection Testing ------");

        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return;

        }

        System.out.println("Oracle JDBC Driver Registered!");

        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@lab-scan.tigeritbd.com:1521/gaidb.tigeritbd.com",
                    "gai_ev36",
                    "gai_ev36");

            if (connection != null) {
                Statement stmt = connection.createStatement();
                stmt.execute(query);
                ResultSet rs = stmt.getResultSet();
                ArrayList<EvType> typeList = new ArrayList<EvType>();


                System.out.println(String.class.getName());

                ArrayList<ColumnInfo> infoList = new ArrayList<ColumnInfo>();
                ResultSetMetaData rsMeta = rs.getMetaData();

                int cc = rsMeta.getColumnCount();
                for (int i = 1; i <= cc; i++) {
                    System.out.println(i + "  " + rsMeta.getColumnName(i) +
                            " " + rsMeta.getColumnTypeName(i));

                    infoList.add(new ColumnInfo(rsMeta.getColumnName(i), rsMeta.getColumnTypeName(i)));
                }

                /*while (rs.next()) {
                    long id = rs.getLong("id");
                    String typeRu = rs.getString("TYPE_RU");
                    String typeEn = rs.getString("TYPE_RU");
                    String typeTj = rs.getString("TYPE_RU");

                    typeList.add(new EvType(id, typeEn, typeRu, typeTj));
                }*/

                try {


                    Font fontTitle = new Font(28, "reportTj", "/fonts/reportTjBold.ttf", Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true);
                    fontTitle.setBold(true);
                    //Font fontTitle = new Font(28, "Times New Roman Tj", true, false, false);
                    //fontTitle.setPdfFontEmbedded(true);

                    Font fontDetail = new Font(14, "reportTj", "/fonts/reportTj.ttf", Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true);

                    //Font fontDetail = new Font(14, "Times New Roman Tj", false, false, false);
                    fontDetail.setPdfFontEmbedded(true);
                    fontDetail.setPdfFontEncoding(Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing);
                    fontDetail.setPdfFontName("/fonts/reportTj.ttf");

                    Font fontDetailHeader = new Font(14, "reportTj", "/fonts/reportTjBold.ttf", Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing, true);
                    fontDetailHeader.setBold(true);
                    //Font fontDetailHeader = new Font(14, "Times New Roman Tj", true, false, false);
                    fontDetailHeader.setPdfFontEmbedded(true);
                    fontDetailHeader.setPdfFontEncoding(Font.PDF_ENCODING_Identity_H_Unicode_with_horizontal_writing);
                    fontDetailHeader.setPdfFontName("/fonts/reportTjBold.ttf");


                    Style titleStyle = new StyleBuilder(false).setFont(fontTitle).setHorizontalAlign(HorizontalAlign.CENTER).build();
                    Style headerStyle = new StyleBuilder(false).setFont(fontDetailHeader).setHorizontalAlign(HorizontalAlign.LEFT).setBorder(Border.NO_BORDER()).build();
                    Style detailStyle = new StyleBuilder(false).setFont(fontDetail).setHorizontalAlign(HorizontalAlign.LEFT).setBorder(Border.THIN()).build();


                    FastReportBuilder drb = new FastReportBuilder();


                    for (ColumnInfo info : infoList) {
                        int width = 50;
//                        if (info.getType().equals(Long.class.getName())) {
//                            width = 10;
//                        }

                        drb.addColumn(info.getName(), info.getName(), info.getType(), width);
                    }

                    drb.addAutoText(
                            AutoText.AUTOTEXT_PAGE_X_SLASH_Y,
                            AutoText.POSITION_FOOTER,
                            AutoText.ALIGNMENT_CENTER
                    );

                    JRExpression expr = new JRDesignExpression();


                    DynamicReport dr = drb
                            .setTitle("$P{Title}", true)
                            .setDefaultStyles(titleStyle, detailStyle, headerStyle, detailStyle)
                            .setSubtitle("This report was generated by test user")
                            .setPrintBackgroundOnOddRows(false)
                            .setUseFullPageWidth(true)
                            .setQuery(query, DJConstants.QUERY_LANGUAGE_SQL)
                            .build();







                    dr.addParameter("Title", String.class.getName());

                    //Parameter p = dr.getParameters().get(0);





                    JRDataSource ds = new JRResultSetDataSource(rs);
                    //JRDataSource ds = new JRBeanCollectionDataSource(typeList);
                    HashMap<String, String> pps = new HashMap<String, String>();
                    pps.put("Title", title);

                    JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds, pps);

                    JasperExportManager.exportReportToPdfFile(jp, "1.pdf");
                    DynamicJasperHelper.generateJRXML(dr, new ClassicLayoutManager(), null, "UTF-8", "1.jrxml");



                    JasperViewer.viewReport(jp);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


//               JasperReportBuilder report = DynamicReports.report();
//
//                report.columns(
//                        Columns.column("Type Id", "id", DataTypes.longType()),
//                        Columns.column("Type RU", "TYPE_RU", DataTypes.stringType()),
//                        Columns.column("Type TJ", "TYPE_TJ", DataTypes.stringType()),
//                        Columns.column("Type EN", "TYPE_EN", DataTypes.dateType())
//                );
//
//
//
//                report.title(Components.text("SimpleReportExample").setHorizontalAlignment(HorizontalAlignment.CENTER));
//                report.pageFooter(Components.pageXofY()); //show page number on the page footer
//                report.setDataSource("select * from ev_type", connection);
//
//                try {
//                    report.show();
//                    report.toPdf(new FileOutputStream("report.pdf"));
//                    report.toExcelApiXls(new FileOutputStream("report.xls"));
//                }catch (DRException ex){
//                    ex.printStackTrace();
//                }catch (IOException ioex){
//                    ioex.printStackTrace();
//                }


            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;

        }
    }


    public void generateFromJrxml(String title, String filePath){
        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@lab-scan.tigeritbd.com:1521/gaidb.tigeritbd.com",
                    "gai_ev36",
                    "gai_ev36");

            if (connection != null) {
//                DynamicReportBuilder drb = new DynamicReportBuilder();
//                drb.setTemplateFile(filePath);
//
//
//                DynamicReport dr = drb.build();
//
//                System.out.println(dr.getQuery());

                HashMap<String, Object> pps = new HashMap<String, Object>();
                pps.put("Title", title);


                //JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), pps);

                try {


                    JasperReport report = JasperCompileManager.compileReport("1.jrxml");
                    JasperPrint jp = JasperFillManager.fillReport(report, pps, connection);
                    JasperExportManager.exportReportToPdfFile(jp, "gen-from-jrxml.pdf");
                    JasperViewer.viewReport(jp);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
